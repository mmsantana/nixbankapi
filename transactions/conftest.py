from pytest_factoryboy import register

from transactions.test.factories import UsuarioFactory

register(UsuarioFactory)
