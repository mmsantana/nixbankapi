from rest_framework import serializers

from transactions.models import Usuario, Transferencia


class UsuarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Usuario
        fields = ['nome', 'cnpj']

    def to_representation(self, obj):
        return {
            "id": obj.id,
            "nome": obj.nome,
            "cnpj": obj.cnpj,
        }


class TransferenciaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transferencia
        fields = [
            'id', 'usuario', 'pagador_nome', 'pagador_banco', 'pagador_agencia', 'pagador_conta',
            'beneficiario_nome', 'beneficiario_banco', 'beneficiario_agencia', 'beneficiario_conta',
            'valor'
        ]

    def to_representation(self, obj):
        return {
            "id": obj.id,
            "data_criacao": obj.data_criacao,
            "usuario": obj.usuario.id,
            "pagador_nome": obj.pagador_nome,
            "pagador_banco": obj.pagador_banco,
            "pagador_agencia": obj.pagador_agencia,
            "pagador_conta": obj.pagador_conta,
            "beneficiario_nome": obj.beneficiario_nome,
            "beneficiario_banco": obj.beneficiario_banco,
            "beneficiario_agencia": obj.beneficiario_agencia,
            "beneficiario_conta": obj.beneficiario_conta,
            "valor": obj.valor,
            "tipo": obj.tipo,
            "status": obj.status,
        }
