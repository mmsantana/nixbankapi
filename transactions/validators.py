import decimal
import re

from django.core.exceptions import ValidationError


def validate_cnpj(value):
    if not re.match(r'^[0-9]{14}$', value) or not value:
        raise ValidationError("O CNPJ deverá ter 14 dígitos, sem caracteres especiais.")

    return value
