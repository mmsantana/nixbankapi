from django.utils.decorators import method_decorator
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status
from rest_framework.decorators import api_view, action
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination

from transactions.models import Usuario, Transferencia
from transactions.serializers import UsuarioSerializer, TransferenciaSerializer


@method_decorator(name='list', decorator=swagger_auto_schema(
    operation_description="description from swagger_auto_schema via method_decorator"
))
class UsuarioViewSet(viewsets.GenericViewSet):
    """
    Uma ViewSet para listar, retornar transações e deletar, de forma lógica, usuários.
    """
    serializer_class = UsuarioSerializer
    queryset = Usuario.objects.all()
    pagination_class = PageNumberPagination

    @swagger_auto_schema(responses={200: UsuarioSerializer(many=True)})
    def list(self, request, *args, **kwargs):
        queryset = Usuario.objects.filter(active=True)
        page = self.paginate_queryset(queryset)
        serializer = self.serializer_class(page, many=True)
        return self.get_paginated_response(serializer.data)

    def retrieve(self, request, pk=None):
        """
        Método responsável por retornar um objeto do banco de dados.
        """
        user = get_object_or_404(self.queryset, pk=pk, active=True)
        serializer = self.serializer_class(user)
        return Response(serializer.data)

    def create(self, request):
        """
        Método responsável por criar um objeto no banco de dados.
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        """
        Método responsável por deletar logicamente o valor no banco de dados.
        """
        user = get_object_or_404(self.queryset, pk=pk)
        transactions = Transferencia.objects.filter(usuario_id=pk).update(active=False)
        if user:
            user.active = False
            user.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)


class TransferenciaViewSet(viewsets.GenericViewSet):
    """
    Uma ViewSet para listar, retornar transações e deletar, de forma lógica, transações.
    """
    serializer_class = TransferenciaSerializer
    queryset = Transferencia.objects.all()
    pagination_class = PageNumberPagination

    def list(self, request, *args, **kwargs):
        """
        Método responsável por retornar vários objetos do modelo de transferência. Também é possível filtrar por
        data de criação (?data_criacao=YYYY/MM/DD), nome do pagador (?pagador_nome='string') e
        nome do beneficiario (?beneficiario_nome='string').
        """
        query_params = {
            key: value for key, value in request.query_params.items()
            if key in ['data_criacao', 'pagador_nome', 'beneficiario_nome']
        }
        queryset = Transferencia.objects.filter(active=True, **query_params)
        page = self.paginate_queryset(queryset)
        serializer = self.serializer_class(page, many=True)
        if queryset:
            response = self.get_paginated_response(serializer.data)
            response.data.update({'total_valores': sum(transferencia.valor for transferencia in queryset)})
        else:
            response = self.get_paginated_response(serializer.data)
        return response

    def retrieve(self, request, pk=None):
        """
        Método responsável por retornar um objeto do banco de dados.
        """
        user = get_object_or_404(self.queryset, pk=pk, active=True)
        serializer = self.serializer_class(user)
        return Response(serializer.data)

    def create(self, request):
        """
        Método responsável por criar um objeto no banco de dados.
        """
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk=None):
        """
        Método responsável por deletar logicamente o valor no banco de dados.
        """
        user = get_object_or_404(self.queryset, pk=pk)
        if user:
            user.active = False
            user.save()
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_404_NOT_FOUND)
