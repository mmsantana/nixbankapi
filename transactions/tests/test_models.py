from datetime import datetime
from unittest.mock import patch

import pytest
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from transactions.models import Usuario, Transferencia
from transactions.test.factories import UsuarioFactory


class TestModels(TestCase):
    
    def setUp(self) -> None:
        self.user = Usuario.objects.create(
            nome='Nome Teste',
            cnpj='57209358720918'
        )
        self.transaction_data = {
            'usuario_id': self.user.id,
            'pagador_nome': 'Nome Pagador',
            'pagador_banco': 'Banco Pagador',
            'pagador_agencia': 'Agencia Pagador',
            'pagador_conta': 'Conta Pagador',
            'beneficiario_nome': 'Nome Beneficiário',
            'beneficiario_banco': 'Banco Beneficiário',
            'beneficiario_agencia': 'Agencia Beneficiário',
            'beneficiario_conta': 'Conta Beneficiário',
            'valor': 1110.1212312
        }

    def test_create_user_objects(self):

        self.assertEqual(self.user.id, 1)
        self.assertEqual(self.user.nome, 'Nome Teste')
        self.assertEqual(self.user.cnpj, '57209358720918')

    def test_create_user_objects_cpf_is_too_long(self):
        self.assertRaises(
            ValidationError,
            Usuario.objects.create,
            nome='Nome Teste', cnpj='572093587209854324'
        )

    def test_create_user_objects_cpf_has_letters(self):
        self.assertRaises(
            ValidationError,
            Usuario.objects.create,
            nome='Nome Teste', cnpj='57209a58720e18'
        )

    def test_create_user_objects_special_chars_cpf(self):
        self.assertRaises(
            ValidationError,
            Usuario.objects.create,
            nome='Nome Teste', cnpj='47.253.571/0001-00'
        )

    def test_create_transaction_objects(self):
        user = Transferencia.objects.create(**self.transaction_data)

        self.assertEqual(user.usuario_id, 1)
        self.assertEqual(user.pagador_nome, 'Nome Pagador')
        self.assertEqual(user.pagador_banco, 'Banco Pagador')
        self.assertEqual(user.beneficiario_nome, 'Nome Beneficiário')
        self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
        self.assertEqual(user.beneficiario_agencia, 'Agencia Beneficiário')
        self.assertEqual(user.beneficiario_conta, 'Conta Beneficiário')
        self.assertEqual(user.valor, 1110.12)
        self.assertEqual(user.status, 'OK')

    def test_create_transaction_objects_with_negative_value(self):
        self.transaction_data['valor'] = -1
        Transferencia.objects.create(**self.transaction_data)

    def test_create_transaction_objects_CC(self):
        self.transaction_data['beneficiario_banco'] = self.transaction_data['pagador_banco']
        user = Transferencia.objects.create(**self.transaction_data)

        self.assertEqual(user.beneficiario_banco, 'Banco Pagador')
        self.assertEqual(user.tipo, 'CC')

    def test_create_transaction_objects_CC_after_10_hours(self):
        self.transaction_data['beneficiario_banco'] = self.transaction_data['pagador_banco']

        # Alteração da data e horário de criação para 11:00
        created_at = datetime(2020, 2, 11, 11, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Pagador')
            self.assertEqual(user.tipo, 'CC')

    def test_create_transaction_objects_TED_11_am(self):
        # Alteração da data e horário de criação para 11:00
        created_at = datetime(2020, 2, 11, 11, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
            self.assertEqual(user.pagador_banco, 'Banco Pagador')
            self.assertEqual(user.created_at, created_at)
            self.assertEqual(user.tipo, 'TED')

    def test_create_transaction_objects_TED_16_pm(self):
        # Alteração da data e horário de criação para 16:00
        created_at = datetime(2020, 2, 11, 16, 00, 00, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
            self.assertEqual(user.pagador_banco, 'Banco Pagador')
            self.assertEqual(user.created_at, created_at)
            self.assertEqual(user.tipo, 'TED')

    def test_create_transaction_objects_DOC_16_00_01_pm(self):
        # Alteração da data e horário de criação para 16:00:01
        created_at = datetime(2020, 2, 11, 16, 0, 1, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
            self.assertEqual(user.pagador_banco, 'Banco Pagador')
            self.assertEqual(user.created_at, created_at)
            self.assertEqual(user.tipo, 'DOC')

    def test_create_transaction_objects_DOC_09_59_59_pm(self):
        # Alteração da data e horário de criação para 09/59/59
        created_at = datetime(2020, 2, 11, 9, 59, 59, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
            self.assertEqual(user.pagador_banco, 'Banco Pagador')
            self.assertEqual(user.created_at, created_at)
            self.assertEqual(user.tipo, 'DOC')

    def test_create_transaction_objects_DOC_value_5000(self):
        self.transaction_data['valor'] = 5000

        # Alteração da data e horário de criação para 15:59
        created_at = datetime(2020, 2, 11, 15, 59, tzinfo=timezone.utc)
        with patch.object(timezone, 'now', return_value=created_at):
            user = Transferencia.objects.create(**self.transaction_data)

            self.assertEqual(user.beneficiario_banco, 'Banco Beneficiário')
            self.assertEqual(user.pagador_banco, 'Banco Pagador')
            self.assertEqual(user.created_at, created_at)
            self.assertEqual(user.tipo, 'DOC')

    def test_create_transaction_objects_value_100_000_01(self):
        self.transaction_data['valor'] = 100000.01

        user = Transferencia.objects.create(**self.transaction_data)

        self.assertEqual(user.status, 'ERRO')


@pytest.mark.django_db
def test_create_usuario(usuario):
    assert usuario == Usuario.objects.last()
