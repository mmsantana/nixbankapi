import json
from datetime import datetime
from decimal import Decimal
from unittest import skip

from django.urls import reverse
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase, APIClient

from transactions.models import Usuario, Transferencia
from transactions.tests.create_objects import create_users_and_transactions
from transactions.tests.result_test import EXPECTED_RESULT


class TestUsuariosViews(APITestCase):

    def setUp(self) -> None:
        self.client = APIClient()
        self.transactions = create_users_and_transactions()
        self.maxDiff = None
        self.expected_response_on_get = {
            'count': 3,
            'next': None,
            'previous': None,
            'results': [
                {
                    'id': 1,
                    'nome': 'Nome Teste',
                    'cnpj': '41234123412341'
                },
                {
                    'id': 2,
                    'nome': 'Nome Teste',
                    'cnpj': '41231314323412'
                },
                {
                    'id': 3,
                    'nome': 'Nome Teste',
                    'cnpj': '12343131432341'
                },
            ]
        }

    # @skip("Não quero testar")
    def test_usuarios_get(self):
        response = self.client.get(reverse('transactions:usuarios-list'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            self.expected_response_on_get
        )

    # @skip("Não quero testar")
    def test_usuarios_get_with_pagination(self):
        response = self.client.get(f"{reverse('transactions:usuarios-list')}?page=1")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            self.expected_response_on_get
        )

    # @skip("Não quero testar")
    def test_usuarios_get_with_pagination_2(self):
        response = self.client.get(f"{reverse('transactions:usuarios-list')}?page=2")

        self.assertEqual(response.status_code, 404)
        self.assertEqual(
            response.data,
            {'detail': ErrorDetail(string='Invalid page.', code='not_found')}
        )

    # @skip("Não quero testar")
    def test_usuario_post(self):
        data = {
            'cnpj': '91234123312341',
            'nome': 'Nome Teste'
        }
        response = self.client.post(reverse('transactions:usuarios-list'), data)

        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            {
                'id': 4,
                'nome': 'Nome Teste',
                'cnpj': '91234123312341'
            }
        )

    # @skip("Não quero testar")
    def test_get_usuario_1(self):
        response = self.client.get(reverse('transactions:usuarios-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                'id': 1,
                'nome': 'Nome Teste',
                'cnpj': '41234123412341'
            }
        )
        self.assertEqual(
            Usuario.objects.get(pk=1).active,
            True
        )

    # @skip("Não quero testar")
    def test_delete_usuario_1(self):
        Transferencia.objects.create(**self.transactions[0])
        response = self.client.delete(reverse('transactions:usuarios-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            Usuario.objects.get(pk=1).active,
            False
        )
        self.assertEqual(
            [transferencia.active for transferencia in Transferencia.objects.filter(usuario_id=1)],
            [False]
        )

    # @skip("Não quero testar")
    def test_usuarios_get_with_deleted_usuario(self):
        self.client.delete(reverse('transactions:usuarios-detail', kwargs={'pk': 2}))
        response = self.client.get(reverse('transactions:usuarios-list'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.data,
            {
                'count': 2,
                'next': None,
                'previous': None,
                'results': [
                    {
                        'id': 1,
                        'nome': 'Nome Teste',
                        'cnpj': '41234123412341'
                    },
                    {
                        'id': 3,
                        'nome': 'Nome Teste',
                        'cnpj': '12343131432341'
                    },
                ]
            }
        )


class TestTransacoesViews(APITestCase):

    def setUp(self) -> None:
        self.client = APIClient()
        self.transactions = create_users_and_transactions()
        self.maxDiff = None

    # @skip("Não quero testar")
    def test_transacoes_get(self):
        self.transactions[0]['valor'] = 3434.1212312
        for transaction in self.transactions:
            Transferencia.objects.create(**transaction)
        response = self.client.get(reverse('transactions:transferencias-list'))

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 3)
        self.assertEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['results'], EXPECTED_RESULT)
        self.assertEqual(response.data['total_valores'], Decimal('5654.36'))

    # @skip("Não quero testar")
    def test_transacoes_get_with_pagination(self):
        self.transactions[0]['valor'] = 3434.1212312
        for transaction in self.transactions:
            Transferencia.objects.create(**transaction)
        response = self.client.get(f"{reverse('transactions:transferencias-list')}?pag=1")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['count'], 3)
        self.assertEqual(response.data['next'], None)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['results'], EXPECTED_RESULT)

    # @skip("Não quero testar")
    def test_transacao_post(self):
        transaction_data = {
            'usuario': 1,
            'pagador_nome': 'Nome Pagador Novo',
            'pagador_banco': 'Ban',
            'pagador_agencia': '1234',
            'pagador_conta': '123456',
            'beneficiario_nome': 'Nome Beneficiário',
            'beneficiario_banco': '123',
            'beneficiario_agencia': '1234',
            'beneficiario_conta': '123456',
            'valor': Decimal('1110.12'),
        }
        response = self.client.post(reverse('transactions:transferencias-list'), transaction_data)

        transaction_data.update({'id': 1, 'data_criacao': datetime.today().date(), 'tipo': 'DOC', 'status': 'OK'})

        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data,
            transaction_data
        )

    # @skip("Não quero testar")
    def test_get_transacao_1(self):
        self.transactions[0]['valor'] = 3434.12
        Transferencia.objects.create(**self.transactions[0])
        response = self.client.get(reverse('transactions:transferencias-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)

        transaction_data = EXPECTED_RESULT[0]
        transaction_data.update({'id': 1, 'tipo': 'DOC', 'status': 'OK'})

        self.assertEqual(
            response.data,
            transaction_data
        )
        self.assertEqual(
            Transferencia.objects.get(pk=1).active,
            True
        )

    # @skip("Não quero testar")
    def test_delete_transacao_1(self):
        Transferencia.objects.create(**self.transactions[0])
        response = self.client.delete(reverse('transactions:transferencias-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            Transferencia.objects.get(pk=1).active,
            False
        )
