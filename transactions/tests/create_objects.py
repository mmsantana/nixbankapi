from transactions.models import Usuario, Transferencia


def create_users_and_transactions():
    list_of_cnpj = ['41234123412341', '41231314323412', '12343131432341']
    transaction_data = []
    for cnpj in list_of_cnpj:
        user = Usuario.objects.create(nome="Nome Teste", cnpj=cnpj)

        transaction_data.append({
            'usuario_id': user.id,
            'pagador_nome': 'Nome Pagador',
            'pagador_banco': 'Banco Pagador',
            'pagador_agencia': 'Agencia Pagador',
            'pagador_conta': 'Conta Pagador',
            'beneficiario_nome': 'Nome Beneficiário',
            'beneficiario_banco': 'Banco Beneficiário',
            'beneficiario_agencia': 'Agencia Beneficiário',
            'beneficiario_conta': 'Conta Beneficiário',
            'valor': 1110.1212312
        })
    return transaction_data
