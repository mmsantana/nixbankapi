from datetime import datetime
from decimal import Decimal

from django.utils import timezone

# today = timezone.now().today()
# today = f'{today.year}-{today.month}-{today.day}'
today = datetime.today().date()

EXPECTED_RESULT = [
    {
        'id': 1, 'data_criacao': today, 'usuario': 1, 'pagador_nome': 'Nome Pagador', 'pagador_banco': 'Banco Pagador',
        'pagador_agencia': 'Agencia Pagador', 'pagador_conta': 'Conta Pagador', 'beneficiario_nome':
        'Nome Beneficiário', 'beneficiario_banco': 'Banco Beneficiário', 'beneficiario_agencia':
        'Agencia Beneficiário', 'beneficiario_conta': 'Conta Beneficiário',
        'valor': Decimal('3434.12'), 'tipo': 'DOC', 'status': 'OK'
    },
    {'id': 2, 'data_criacao': today,  'usuario': 2, 'pagador_nome': 'Nome Pagador', 'pagador_banco': 'Banco Pagador',
     'pagador_agencia': 'Agencia Pagador', 'pagador_conta': 'Conta Pagador',
     'beneficiario_nome': 'Nome Beneficiário', 'beneficiario_banco': 'Banco Beneficiário',
     'beneficiario_agencia': 'Agencia Beneficiário', 'beneficiario_conta': 'Conta Beneficiário',
     'valor': Decimal('1110.12'), 'tipo': 'DOC', 'status': 'OK'
     },
    {'id': 3, 'data_criacao': today,  'usuario': 3, 'pagador_nome': 'Nome Pagador', 'pagador_banco': 'Banco Pagador',
     'pagador_agencia': 'Agencia Pagador', 'pagador_conta': 'Conta Pagador', 'beneficiario_nome': 'Nome Beneficiário',
     'beneficiario_banco': 'Banco Beneficiário', 'beneficiario_agencia': 'Agencia Beneficiário',
     'beneficiario_conta': 'Conta Beneficiário', 'valor': Decimal('1110.12'), 'tipo': 'DOC', 'status': 'OK'
     }
]
