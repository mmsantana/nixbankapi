from unittest import skip

from django.urls import reverse
from rest_framework.test import APITestCase, APIClient

from transactions.models import Usuario, Transferencia
from transactions.tests.create_objects import create_users_and_transactions


class TestUsuariosUrls(APITestCase):

    def setUp(self) -> None:
        self.client = APIClient()
        create_users_and_transactions()

    # @skip("Não quero testar")
    def test_usuarios_get(self):
        response = self.client.get(reverse('transactions:usuarios-list'))

        self.assertEqual(response.status_code, 200)

    # @skip("Não quero testar")
    def test_usuarios_delete(self):
        response = self.client.delete(reverse('transactions:usuarios-list'))

        self.assertEqual(response.status_code, 405)

    # @skip("Não quero testar")
    def test_usuario_post(self):
        data = {
            'cnpj': '91234123312341',
            'nome': 'Nome Teste'
        }
        response = self.client.post(reverse('transactions:usuarios-list'), data)

        self.assertEqual(response.status_code, 201)

    # @skip("Não quero testar")
    def test_usuario_put(self):
        data = {
            'cnpj': '91234123312341',
            'nome': 'Nome Teste'
        }
        response = self.client.put(reverse('transactions:usuarios-detail', kwargs={'pk': 1}), data)

        self.assertEqual(response.status_code, 405)

    # @skip("Não quero testar")
    def test_usuario_patch(self):
        data = {
            'cnpj': '91234123312341',
            'nome': 'Nome Teste'
        }
        response = self.client.patch(reverse('transactions:usuarios-detail', kwargs={'pk': 1}), data)

        self.assertEqual(response.status_code, 405)

    # @skip("Não quero testar")
    def test_usuario_post_already_exists(self):
        data = {
            'cpf': '41234123412341',
            'nome': 'Nome Teste'
        }
        response = self.client.post(reverse('transactions:usuarios-list'), data)

        self.assertEqual(response.status_code, 400)

    # @skip("Não quero testar")
    def test_get_usuario_1(self):
        response = self.client.get(reverse('transactions:usuarios-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)

    # @skip("Não quero testar")
    def test_delete_usuario_1(self):
        response = self.client.delete(reverse('transactions:usuarios-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 204)
        self.assertEqual(
            Usuario.objects.get(pk=1).active,
            False
        )


class TestTransacoesUrls(APITestCase):

    def setUp(self) -> None:
        self.client = APIClient()
        self.transactions = create_users_and_transactions()

    # @skip("Não quero testar")
    def test_transacoes_get(self):
        response = self.client.get(reverse('transactions:transferencias-list'))

        self.assertEqual(response.status_code, 200)

    # @skip("Não quero testar")
    def test_transacao_post(self):
        transaction_data = {
            'usuario': 1,
            'pagador_nome': 'Nome Pagador Novo',
            'pagador_banco': 'Ban',
            'pagador_agencia': '1234',
            'pagador_conta': '123456',
            'beneficiario_nome': 'Nome Beneficiário',
            'beneficiario_banco': '123',
            'beneficiario_agencia': '1234',
            'beneficiario_conta': '123456',
            'valor': 1110.12,
        }
        response = self.client.post(reverse('transactions:transferencias-list'), transaction_data)

        self.assertEqual(response.status_code, 201)

    # @skip("Não quero testar")
    def test_transacao_put(self):
        self.transactions[0]['valor'] = 1000
        response = self.client.put(
            reverse('transactions:transferencias-detail', kwargs={'pk': 1}), self.transactions[0]
        )

        self.assertEqual(response.status_code, 405)

    # @skip("Não quero testar")
    def test_transacao_patch(self):
        self.transactions[0]['valor'] = 1000
        response = self.client.patch(
            reverse('transactions:transferencias-detail', kwargs={'pk': 1}), self.transactions[0]
        )

        self.assertEqual(response.status_code, 405)

    # @skip("Não quero testar")
    def test_get_transacao_1(self):
        Transferencia.objects.create(**self.transactions[0])
        response = self.client.get(reverse('transactions:transferencias-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 200)

    # @skip("Não quero testar")
    def test_delete_transacao_1(self):
        Transferencia.objects.create(**self.transactions[0])
        response = self.client.delete(reverse('transactions:transferencias-detail', kwargs={'pk': 1}))

        self.assertEqual(response.status_code, 204)
