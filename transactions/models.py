import datetime

from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone

from transactions.validators import validate_cnpj


class AbstractModel(models.Model):
    created_at = models.DateTimeField("Criado em", auto_now_add=True)
    data_criacao = models.DateField("Criado em", default=datetime.date.today)
    updated_at = models.DateTimeField("Atualizado em", auto_now=True)
    active = models.BooleanField("Ativo", default=True)

    class Meta:
        abstract = True


class Usuario(AbstractModel):
    nome = models.CharField("Nome", max_length=128)
    cnpj = models.CharField("CNPJ", max_length=14, validators=[validate_cnpj], unique=True)

    def save(self, *args, **kwargs):
        self.validade_cnpj()
        super(Usuario, self).save(*args, **kwargs)

    def validade_cnpj(self):
        return validate_cnpj(self.cnpj)

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"
        ordering = ['id']


CHOICES_TIPO = (
    ('CC', 'CC'),
    ('TED', 'TED'),
    ('DOC', 'DOC')
)
CHOICES_STATUS = (
    ('OK', 'OK'),
    ('ERRO', 'ERRO')
)


class Transferencia(AbstractModel):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    pagador_nome = models.CharField("Nome do Pagador", max_length=128)
    pagador_banco = models.CharField("Banco do Pagador", max_length=3)
    pagador_agencia = models.CharField("Agencia do Pagador", max_length=4)
    pagador_conta = models.CharField("Conta do Pagador", max_length=6)
    beneficiario_nome = models.CharField("Nome do Beneficiario", max_length=128)
    beneficiario_banco = models.CharField("Banco do Beneficiario", max_length=3)
    beneficiario_agencia = models.CharField("Agencia do Beneficiario", max_length=4)
    beneficiario_conta = models.CharField("Conta do Beneficiario", max_length=6)
    valor = models.DecimalField("Valor", max_digits=15, decimal_places=2, validators=[MinValueValidator(0.01)])

    def save(self, *args, **kwargs):
        self.valor = round(self.valor, 2)
        super(Transferencia, self).save(*args, **kwargs)

    @property
    def tipo(self):
        hour_of_day = timezone.now().minute / 60 + timezone.now().hour + timezone.now().second / 3600
        if self.pagador_banco == self.beneficiario_banco:
            return 'CC'
        elif 10 <= hour_of_day <= 16 and self.valor < 5000:
            return 'TED'
        else:
            return 'DOC'

    @property
    def status(self):
        if self.valor > 100_000.0:
            return 'ERRO'
        return 'OK'

    class Meta:
        verbose_name = "Transferencia"
        verbose_name_plural = "Transferencias"
        ordering = ['id']
