from django.urls import path, include
from rest_framework import routers

from transactions.views import UsuarioViewSet, TransferenciaViewSet

app_name = 'transactions'
router = routers.DefaultRouter()
router.register('usuarios', UsuarioViewSet, basename='usuarios')
router.register('transferencias', TransferenciaViewSet, basename='transferencias')

urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
