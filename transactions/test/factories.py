import factory
from factory.django import DjangoModelFactory
from faker import Faker


from transactions.models import Usuario

fake = Faker('pt_BR')


class UsuarioFactory(DjangoModelFactory):

    class Meta:
        model = Usuario

    nome = fake.name()
    cnpj = factory.LazyAttribute(lambda x: fake.cnpj().replace('.', '').replace('-', '').replace('/', ''))
