# Transactions API
Esse documento traz todos os passos para iniciar e utilizar a aplicação.
## Setup da Aplicação

A aplicação utiliza Python como liguagem. Para continuar com o setup, 
baixe a versão 3.6 do Python ou superior.

Para inciar a aplicação será necessário criar um ambiente virtual.
Para criar o ambiente virtual vá ao terminal e digite os seguintes 
comandos:

`pip install virtualenv`

`virtualenv venv`

Ative o ambiente virtual:

* Ubuntu: `source venv/bin/activate`
* Windows: `env\Scripts\activate`

Instale os pacotes necessários da aplicação:

`pip install -r requirements.txt`

Realize as migrações:

`python manage.py makemigrations`

`python manage.py migrate`

Inicie o servidor:

`python manage.py runserver`

## Documentação da API
A documentação foi realizada utilizando o SWAGGER. Para acessá-la, 
acesse o link:

`localhost:8000/redoc`

ou

`http://localhost:8000/swagger/`

## Endpoints da API
A API possui os seguintes endpoints, que também estão apresentados
na documentação da API:

`localhost:8000/api/usuarios`

`localhost:8000/api/usuarios/<pk>`

`localhost:8000/api/tranferencias/`

`localhost:8000/api/tranferencias/<pk>`